/**
 * 
 */
package com.wipro.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

/**
 * @author CH301729
 *
 */
@Controller
public class S3Controller {
	static final int BUFFER_SIZE = 4096;
	private static final String SUFFIX = "/";

	@RequestMapping(value = "/encode", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JSONObject encodeFile(
			@RequestBody TranscodeBean trsBean) {

		// System.out.println("$$$$$$$$$$$$$$" + trsBean.getUrl() + "<>"+
		// trsBean.getPath());

		ConvertVideo ct = new ConvertVideo();
		boolean result = ct.convertVideo(trsBean);

		JSONObject jobj = new JSONObject();
		if (result) {
			jobj = upload(trsBean.getPath(), trsBean.getBucketName(),
					trsBean.getFolderName());

		} else {
			jobj.put("result", "FAILED");
		}
		return jobj;
	}

	public JSONObject upload(String path, String bucketName, String folderName) {
		JSONObject jobj = new JSONObject();
		try {
			AWSCredentials credentials = new BasicAWSCredentials(
					"AKIAJN2BGKKOEYLBJZDQ",
					"kBhuRhnDWZODaUyIQX3pVfq0Y79OKLRkOL9Q5RQA");
			ClientConfiguration clientConfig = new ClientConfiguration();
			clientConfig.setProtocol(Protocol.HTTP);

			// create a client connection based on credentials
			AmazonS3 s3client = new AmazonS3Client(credentials, clientConfig);
			if (!s3client.doesBucketExist(bucketName)) {
				s3client.createBucket(bucketName);
				createFolder(bucketName, folderName, s3client);
			}
			String name = path.substring(path.lastIndexOf("/") + 1,
					path.length());
			// upload file to folder and set it to public
			String fileName = folderName + SUFFIX + name;
			// System.out.println("file name to upload " + fileName);
			s3client.putObject(new PutObjectRequest(bucketName, fileName,
					new File(path)));
			jobj.put("result", "SUCCESS");
		} catch (AmazonServiceException ase) {
			jobj.put("result", "FAILED");
		} catch (AmazonClientException ace) {
			jobj.put("result", "FAILED");
		} catch (Exception e) {
			e.printStackTrace();
			jobj.put("result", "FAILED");
		}
		return jobj;
	}

	public static void createFolder(String bucketName, String folderName,
			AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
				folderName + SUFFIX, emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}
}
