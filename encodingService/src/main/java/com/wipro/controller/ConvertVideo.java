package com.wipro.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * CreateThumbnail class for executing the ffmpeg command.
 *
 * @author
 * @version 1.0
 * @see
 */
public class ConvertVideo {

	public boolean convertVideo(TranscodeBean trsBean) {

		// Execute cmd Command to fragment video file
		// Before executing set the path in system variable...
		/*
		 * ProcessBuilder builder = new ProcessBuilder( "cmd.exe", "/c",
		 * "ffmpeg -ss 00:00:37 -t 00:00:00.04 -i  E:\\offlinepkgr\\INPUT\\" +
		 * str +
		 * ".mp4 -s 392x220 -r 25.0 -f image2 E:\\apache-tomcat-7.0.42\\webapps\\Image\\"
		 * + str + ".jpg ");
		 */

		String str = "ffmpeg -i " + trsBean.getUrl() + " " + trsBean.getPath();
		// System.out.println("##############" + str);

		try {
			executeLinuxCommand(str);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private String executeLinuxCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}

}
