package com.wipro.ivid;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.wipro.encrypt.ExecuteCommand;
import com.wipro.encrypt.XmlConfig;
import com.wipro.ivid.util.AESProtection;
import com.wipro.ivid.util.DRMNoWindowsProtection;
import com.wipro.ivid.util.NormalProtection;
import com.wipro.model.Convert;
 
/**
 * ConvertProcess class for all conversion activity.
 *
 * @author     
 * @version     1.0
 * @see
 */
public class ConvertProcessConcurrent {

	String catname;
	XmlConfig xml ;
	ExecuteCommand exe ;
	Logger log = Logger.getLogger(ConvertProcessConcurrent.class);
	 /** 
     * Convert process method
     *
     * @param 			convert    
     * @return          true if all conversion process completed successfully. 
     *               	false if any type of error or exception occurred during
     *               	the conversion process.
     * @see             XmlConfig, ExecuteCommand, DAOForConversion, Thumbnail
     * @version         1.0
     */	
	public boolean convertProcess(Convert convert) {
		if(convert != null){ 		 	
			String name = convert.getName();
			//System.out.println("<<<<<<<<<<<<<"+convert.getAes());
			//System.out.println("<<<<<<<<<<<<<"+convert.getDrmplain());
			//System.out.println("<<<<<<<<<<<<<"+convert.getDrmnowindows());
 			//System.out.println("inside conversion method"+name);
			// .......Check whether the file downloaded or not......... 
			File f = new File("/ivid/encodingService/offlinepkgr/INPUT/" +name+".mp4");
			
			//System.out.println("File is =>>"+f.toString());
			//System.out.println("File path is : " + System.getenv("VIDEO_LOG") + "\\INPUT\\" +name+".mp4");
			
			
			if (!f.exists()) {
				log.info("input file(video) name is not present");
				return false;
			}			
							
			// System.out.println(""+convert+"<>"+ name);
			//Creating an object of the thread
			NormalProtection   normalThread = new NormalProtection(convert, name);
			AESProtection   aesThread = new AESProtection(convert, name);
			DRMNoWindowsProtection   drmnowinThread = new DRMNoWindowsProtection(convert, name);
			
	        Thread thread1 = new Thread(normalThread);	        	        
	        Thread thread3 = new Thread(aesThread);	       	       
	        Thread thread5 = new Thread(drmnowinThread);
	        //Start all  five threads.
	        thread1.start();
	        thread3.start();	      
	        thread5.start();	        
			
			// Create Thumbnail ....................................
 			/*
 			 * 
 			  ct = new CreateThumbnail();
 			 
			if (ct.createThumbnail(name)) {
				System.out.println("Thumbnail created successfully.");
			} else {
				System.out.println("Thumbnail not created successfully.");
			}
			
			*/

			// .............Delete the configuration file.........
			//xml.delete();
		}
		return true;
	}

	/** 
     * getDateAndTime   method gives the current date and time.
     *
     * @return          String of current date and time.  
     *               
     * @see             Thumbnail
     * @version         1.0
     */
	public String getDateAndTime() {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
		// System.out.println("Current Date: " + ft.format(dNow));
		return ft.format(dNow);
	}

	 


}
