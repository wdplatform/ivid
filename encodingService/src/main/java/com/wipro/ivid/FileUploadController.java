package com.wipro.ivid;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wipro.ivid.util.CommonUtils;
import com.wipro.model.Convert;
import com.wipro.model.EncodeBean;
 
@Controller
public class FileUploadController {
    
	Logger log = Logger.getLogger(FileUploadController.class);
	static final int BUFFER_SIZE = 4096;
	

	@RequestMapping(method = RequestMethod.GET, value = "/upload")
	public String provideUploadInfo() {
		
		return "uploadForm";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/uploadFile1")
	public String handleFileUpload(@RequestParam("name") String name,
								   @RequestParam("file") MultipartFile file,
								   RedirectAttributes redirectAttributes) {
		
		
		if (!file.isEmpty()) {
			try {
				// upload(file);
				// File uploadedFile = new File("D:/chanda/saveVideo/" + file.getOriginalFilename());
				File uploadedFile = new File("/var/lib/tomcat7/webapps/savevideo/" + file.getOriginalFilename());
				log.info("video got svaed in /var/lib/tomcat7/webapps/savevideo/ path");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(uploadedFile));
                FileCopyUtils.copy(file.getInputStream(), stream);
                stream.close();
                
				CommonUtils.upload(uploadedFile.getAbsolutePath());
				if(uploadedFile != null && uploadedFile.exists()){
					uploadedFile.delete();
				}
				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + name + "!");
			}
			catch (Exception e) {
				redirectAttributes.addFlashAttribute("message",
						"You failed to upload " + name + " => " + e.getMessage());
			}
		}
		else {
			redirectAttributes.addFlashAttribute("message",
					"You failed to upload " + name + " because the file was empty");
		}

		return "redirect:upload";
	}

	@RequestMapping(value = "/encode", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody boolean encodeFile(
			@RequestBody EncodeBean trsBean) {

		// System.out.println("$$$$$$$$$$$$$$" + trsBean.getUrl() + "<>"+
		// trsBean.getPath());

		EncodeVideo ct = new EncodeVideo();
		return ct.convertVideo(trsBean);
	}
	
	

	@RequestMapping(value = "/transcode", method = RequestMethod.POST,produces="application/json" )      
    public @ResponseBody String transcodeFile( @RequestBody  Convert convert )   {                             
          
        //  System.out.println("in post_convert:9090");     
          JSONObject j = new JSONObject();
          j.put("msg","Your information is received : 9090"); 
          
          JSONObject j2 = new JSONObject();
         // System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%in convert url method "+convert.getName());
          ConvertProcessConcurrent cnb = new ConvertProcessConcurrent();
          boolean jb = cnb.convertProcess(convert); 
          if(jb){
        	  return(CommonUtils.uploadFolder(convert.getName()));
                
          }else{
              //  System.out.println("Convertion not Completed.");
                j2.put("msg", "fail");
          }           
          return j.toString();    
          
    }

}