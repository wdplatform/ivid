package com.wipro.ivid.util;

import com.wipro.encrypt.ExecuteCommand;
import com.wipro.encrypt.XmlConfig;
import com.wipro.model.Convert;

public class NormalProtection implements Runnable{
	String catname;
	XmlConfig xml ;
	ExecuteCommand exe ;
	Convert convert = new Convert();
	String name;



	public NormalProtection(Convert convert, String name){
		this.convert = convert;
		this.name = name;

	}


	@Override
	public void run() {
		xml = new XmlConfig();
		try{
			if(convert.getNormal() != null){				 
				if(convert.getNormal().equalsIgnoreCase("NORMAL")){
					String config = xml.normalXmlCreator(name);
					if (config != null) {
						// ..............Execute Command......................

                              
                              
                              if(new ExecuteCommand().executeCommand(config)){
                                    CommonUtils.uploadFolder("/var/lib/tomcat7/webapps/savevideo/"+name);
                              }

					}
				} 
			}
		}catch(Exception e){
			e.printStackTrace();

		} 

	}

}
