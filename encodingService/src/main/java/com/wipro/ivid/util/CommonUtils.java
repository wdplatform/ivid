package com.wipro.ivid.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import org.json.JSONObject;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;

public class CommonUtils {

	private static final String SUFFIX = "/";
	
/*	
	
	public static JSONObject upload(String path, String bucketName, String folderName) {
		JSONObject jobj = new JSONObject();
		try {
			AWSCredentials credentials = new BasicAWSCredentials(
					"AKIAJN2BGKKOEYLBJZDQ",
					"kBhuRhnDWZODaUyIQX3pVfq0Y79OKLRkOL9Q5RQA");
			ClientConfiguration clientConfig = new ClientConfiguration();
			clientConfig.setProtocol(Protocol.HTTP);

			// create a client connection based on credentials
			AmazonS3 s3client = new AmazonS3Client(credentials, clientConfig);
			if (!s3client.doesBucketExist(bucketName)) {
				s3client.createBucket(bucketName);
				createFolder(bucketName, folderName, s3client);
			}
			String name = path.substring(path.lastIndexOf("/") + 1,
					path.length());
			// upload file to folder and set it to public
			String fileName = folderName + SUFFIX + name;
			// System.out.println("file name to upload " + fileName);
			s3client.putObject(new PutObjectRequest(bucketName, fileName,
					new File(path)));
			jobj.put("result", "SUCCESS");
		} catch (AmazonServiceException ase) {
			jobj.put("result", "FAILED");
		} catch (AmazonClientException ace) {
			jobj.put("result", "FAILED");
		} catch (Exception e) {
			e.printStackTrace();
			jobj.put("result", "FAILED");
		}
		return jobj;
	}*/
	public static void createFolder(String bucketName, String folderName,
			AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
				folderName + SUFFIX, emptyContent, metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}
	
	public static String uploadFolder(String name) {
		JSONObject jobj = new JSONObject();
		String bucketName = "wipro-digital-ivid";
		String folderName = "media";
		try {
			AWSCredentials credentials = new BasicAWSCredentials(
					"AKIAJN2BGKKOEYLBJZDQ",
					"kBhuRhnDWZODaUyIQX3pVfq0Y79OKLRkOL9Q5RQA");
			ClientConfiguration clientConfig = new ClientConfiguration();
			clientConfig.setProtocol(Protocol.HTTP);
			// create a client connection based on credentials
			AmazonS3 s3client = new AmazonS3Client(credentials, clientConfig);
			if (!s3client.doesBucketExist(bucketName)) {
				s3client.createBucket(bucketName);
				createFolder(bucketName, folderName, s3client);
			}
			// upload file to folder and set it to public
			String fileName = folderName + SUFFIX + name;
			// System.out.println("file name to upload " + fileName);
			TransferManager tx = new TransferManager(s3client);
			File file = new File("/var/lib/tomcat7/webapps/Videos/"+name);          //"D:/chanda/uploadVideo/text;

			MultipleFileUpload myUpload = tx.uploadDirectory(bucketName,
					fileName, file, true);
			// You can poll your transfer's status to check its progress

			/*if (myUpload.isDone() == false) {
				System.out.println("Transfer: " + myUpload.getDescription());
				System.out.println("  - State: " + myUpload.getState());
				System.out.println("  - Progress: "
						+ myUpload.getProgress().getBytesTransferred());
			}
*/
			// Transfers also allow you to set a <code>ProgressListener</code>
			// to receive
			// asynchronous notifications about your transfer's progress.
			
						/*myUpload.addProgressListener(new com.amazonaws.event.ProgressListener() {
			
							@Override
							public void progressChanged(
									com.amazonaws.event.ProgressEvent progressEvent) {
								System.out.println("Transferred bytes: "
										+ progressEvent.getBytesTransferred());
							}
						});*/

			/*
			 * addProgressListener(new ProgressListener() { public void
			 * progressChanged(ProgressEvent event) {
			 * System.out.println("Transferred bytes: " +
			 * event.getBytesTransfered()); } });
			 */

			// Or you can block the current thread and wait for your transfer to
			// to complete. If the transfer fails, this method will throw an
			// AmazonClientException or AmazonServiceException detailing the
			// reason.
			
			myUpload.waitForCompletion();
			
			// After the upload is complete, call shutdownNow to release the
			// resources.
			tx.shutdownNow();
			jobj.put("result", "SUCCESS");
		} catch (AmazonServiceException ase) {
			jobj.put("result", "FAILED");
		} catch (AmazonClientException ace) {
			jobj.put("result", "FAILED");
		} catch (Exception e) {
			e.printStackTrace();
			jobj.put("result", "FAILED");
		}
		return jobj.toString();
	}



	public static JSONObject upload(String path) {
		JSONObject jobj = new JSONObject();
		try {
			AWSCredentials credentials = new BasicAWSCredentials(
					"AKIAJN2BGKKOEYLBJZDQ",
					"kBhuRhnDWZODaUyIQX3pVfq0Y79OKLRkOL9Q5RQA");
			ClientConfiguration clientConfig = new ClientConfiguration();
			clientConfig.setProtocol(Protocol.HTTP);


			

			// create a client connection based on credentials
			String name ="";
			AmazonS3 s3client = new AmazonS3Client(credentials, clientConfig);
			if(path.contains("/")){
				name = path.substring((path.lastIndexOf("/"))+ 1,
						path.length());
			}
			else
				name = path.substring((path.lastIndexOf("\\"))+ 1,
						path.length());

			// upload file to folder and set it to public
			String fileName = "media" + SUFFIX + name;
			s3client.putObject(new PutObjectRequest("wipro-digital-ivid", fileName,
					new File(path)));
			jobj.put("result", "SUCCESS");
		} catch (AmazonServiceException ase) {
			jobj.put("result", "FAILED");
		} catch (AmazonClientException ace) {
			jobj.put("result", "FAILED");
		} catch (Exception e) {
			e.printStackTrace();
			jobj.put("result", "FAILED");
		}
		return jobj;
	}


}
