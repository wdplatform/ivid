package com.wipro.ivid.util;

import com.wipro.encrypt.ExecuteCommand;
import com.wipro.encrypt.XmlConfig;
import com.wipro.model.Convert;

public class DRMNoWindowsProtection implements Runnable{
	
	String catname;
	XmlConfig xml ;
	ExecuteCommand exe ;
	Convert convert = new Convert();
	String name;
	
	public DRMNoWindowsProtection(Convert convert, String name){
		this.convert = convert;
		this.name = name;
	}
	
	@Override
	public void run() {
		xml = new XmlConfig();
		try{
			if(convert.getDrmnowindows() != null){				 
				if(convert.getDrmnowindows().equalsIgnoreCase("DRMNOWINDOWS")){
					String config = xml.drmNoWindowsXmlCreator(name);
					if (config != null) {
						// ..............Execute Command......................
						exe = new ExecuteCommand(); 
						exe.executeCommand(config);
					}
				}
			}	
		}catch(Exception e){
			e.printStackTrace();
			
		}


		
	}
	
}


