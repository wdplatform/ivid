package com.wipro.ivid.util;

import com.wipro.encrypt.ExecuteCommand;
import com.wipro.encrypt.XmlConfig;
import com.wipro.model.Convert;

public class AESProtection implements Runnable{
	
	String catname;
	XmlConfig xml ;
	ExecuteCommand exe ;
	Convert convert;
	String name;
	
	public AESProtection(Convert convert, String name){
		this.convert = convert;
		this.name = name;
	}
	
	@Override
	public void run() {
		xml = new XmlConfig();
		try{
			if(convert.getAes() != null){				 
				if(convert.getAes().equalsIgnoreCase("AES")){
					String config = xml.aesXmlCreator(name);
					if (config != null) {
						// ..............Execute Command......................
						exe = new ExecuteCommand(); 
						exe.executeCommand(config);
						
					}
				}
			}	
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	}
}
