package com.wipro.model;

public class Convert {
	
	private String name;
	private String normal;
	private String phls;
	private int normalId;
	private int phlsId;
	private String aes;
	private int aesId;
	private String drmplain;
	private int drmplainId;
	private String drmnowindows;
	private int drmnowindowsId;
	private String inputFile;
	private String uploadBucket;
	
	public String getInputFile() {
		return inputFile;
	}
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	public String getUploadBucket() {
		return uploadBucket;
	}
	public void setUploadBucket(String uploadBucket) {
		this.uploadBucket = uploadBucket;
	}
	
	
	
	public String getDrmplain() {
		return drmplain;
	}
	public void setDrmplain(String drmplain) {
		this.drmplain = drmplain;
	}
	public int getDrmplainId() {
		return drmplainId;
	}
	public void setDrmplainId(int drmplainId) {
		this.drmplainId = drmplainId;
	}
	public String getDrmnowindows() {
		return drmnowindows;
	}
	public void setDrmnowindows(String drmnowindows) {
		this.drmnowindows = drmnowindows;
	}
	public int getDrmnowindowsId() {
		return drmnowindowsId;
	}
	public void setDrmnowindowsId(int drmnowindowsId) {
		this.drmnowindowsId = drmnowindowsId;
	}
	public String getAes() {
		return aes;
	}
	public void setAes(String aes) {
		this.aes = aes;
	}
	public int getAesId() {
		return aesId;
	}
	public void setAesId(int aesId) {
		this.aesId = aesId;
	}
	public int getPhlsId() {
		return phlsId;
	}
	public void setPhlsId(int phlsId) {
		this.phlsId = phlsId;
	}
	public int getNormalId() {
		return normalId;
	}
	public void setNormalId(int normalId) {
		this.normalId = normalId;
	}
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNormal() {
		return normal;
	}
	public void setNormal(String normal) {
		this.normal = normal;
	}
	public String getPhls() {
		return phls;
	}
	public void setPhls(String phls) {
		this.phls = phls;
	}
	 
	
	
	

}

