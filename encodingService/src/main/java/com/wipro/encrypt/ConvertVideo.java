package com.wipro.encrypt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.wipro.model.TranscodeBean;


/**
 * CreateThumbnail class for executing the ffmpeg command.
 *
 * @author     
 * @version     1.0
 * @see
 */
public class ConvertVideo {

	public boolean convertVideo() {
		
		TranscodeBean trsBean  = new TranscodeBean();

		// Execute cmd Command to fragment video file
		// Before executing set the path in system variable...
		/*ProcessBuilder builder = new ProcessBuilder(
				"cmd.exe",
				"/c",
				"ffmpeg -ss 00:00:37 -t 00:00:00.04 -i  E:\\offlinepkgr\\INPUT\\"
						+ str
						+ ".mp4 -s 392x220 -r 25.0 -f image2 E:\\apache-tomcat-7.0.42\\webapps\\Image\\"
						+ str + ".jpg ");*/
		
		String str = "ffmpeg -i "+trsBean.getDownloadUrl()+" "+trsBean.getFilePath();
		
		executeLinuxCommand(str);
		
		ProcessBuilder builder = new ProcessBuilder(
				"cmd.exe",
				"/c",str);
			//"ffmpeg -i D:\\BBVIDEO\\MI.mp4 -r 30 -s 960x540 -f h264 D:\\BBVIDEO\\MI4.flv");
			//"ffmpeg -i D:\\BBVIDEO\\MI.mp4 -c:v libx264 -b:v 5000k D:\\BBVIDEO\\MI3.flv");//cut video
			//"ffmpeg -i "+trsBean.getDownloadUrl()+" "+trsBean.getFilePath());
			//"ffmpeg -i D:\\BBVIDEO\\MI.mp4 D:\\BBVIDEO\\MI6.flv"); //Convert video file
		builder.redirectErrorStream(true);
		Process p = null;
		try {
			p = builder.start();

		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	private String executeLinuxCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
                            new BufferedReader(new InputStreamReader(p.getInputStream()));

                        String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}

}
