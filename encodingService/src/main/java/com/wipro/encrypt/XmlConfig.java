package com.wipro.encrypt;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * XmlConfig class for creating configuration files.
 *
 * @author       
 * @version     1.0
 * @see
 */
public class XmlConfig {

    File file;

	public XmlConfig() {

	}
	
	/** 
     * normalXmlCreator method
     *
     * @param 			String vname
     * @return          String 
     * @see              
     * @version         1.0
     */	
	public String normalXmlCreator(String vname) {


		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element config = doc.createElement("config");
			doc.appendChild(config);

			// set Elementibute to in_path element
			Element in_path = doc.createElement("in_path");
			//in_path.appendChild(doc.createTextNode("E:\\offlinepkgr\\INPUT\\" + vname + ".mp4"));
			in_path.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/INPUT/" + vname + ".mp4"));
			config.appendChild(in_path);

			// set attribute to out_type element
			Element out_type = doc.createElement("out_type");
			out_type.appendChild(doc.createTextNode("hls"));
			config.appendChild(out_type);

			// set attribute to out_path element
			Element out_path = doc.createElement("out_path");
			out_path.appendChild(doc
					.createTextNode("/var/lib/tomcat7/webapps/Videos/"+vname));// output path																					 
			config.appendChild(out_path);

			// set attribute to name element
			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(vname));
			config.appendChild(name);

			// set attribute to name element
			Element frag_dur = doc.createElement("frag_dur");
			frag_dur.appendChild(doc.createTextNode("4"));
			config.appendChild(frag_dur);

			// set attribute to name element
			Element target_dur = doc.createElement("target_dur");
			target_dur.appendChild(doc.createTextNode("6"));
			config.appendChild(target_dur);
			 

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			file = new File("/ivid/encodingService/offlinepkgr/" + vname + "_normal.xml");// Change the
																	// file name
																	// dynamically.........................
			StreamResult result = new StreamResult(file);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);


		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return null;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return null;
		}

		return file.getName();
	}
	
	/** 
     * protectedXmlCreator method
     *
     * @param 			String vname
     * @return          String 
     * @see              
     * @version         1.0
     */	
	public String protectedXmlCreator(String vname) {


		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element config = doc.createElement("config");
			doc.appendChild(config);

			// set Elementibute to in_path element
			Element in_path = doc.createElement("in_path");
			in_path.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/INPUT/" + vname + ".mp4"));
			config.appendChild(in_path);

			// set attribute to out_type element
			Element out_type = doc.createElement("out_type");
			out_type.appendChild(doc.createTextNode("hls"));
			config.appendChild(out_type);

			// set attribute to out_path element
			Element out_path = doc.createElement("out_path");
			out_path.appendChild(doc
					.createTextNode("/var/lib/tomcat7/webapps/Videos/Protected"));// output
																					 
			config.appendChild(out_path);

			// set attribute to name element
			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(vname));
			config.appendChild(name);

			// set attribute to drm element
			Element drm = doc.createElement("drm");
			config.appendChild(drm);

			// set attribute to name element
			Element drm_sys = doc.createElement("drm_sys");
			drm_sys.appendChild(doc.createTextNode("AAXS"));
			config.appendChild(drm_sys);

			// set attribute to name element
			Element frag_dur = doc.createElement("frag_dur");
			frag_dur.appendChild(doc.createTextNode("15"));
			config.appendChild(frag_dur);

			// set attribute to name element
			Element target_dur = doc.createElement("target_dur");
			target_dur.appendChild(doc.createTextNode("6"));
			config.appendChild(target_dur);

			// set attribute to name element 
			Element lic_svr_url = doc.createElement("lic_svr_url");
			lic_svr_url.appendChild(doc
					.createTextNode("http://primetime.aaxs.adobe.com"));
			config.appendChild(lic_svr_url);
			  
			// set attribute to name element 
			Element lic_svr_cer = doc.createElement("lic_svr_cer");
			lic_svr_cer.appendChild(doc
					.createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_license_server.der"));
			config.appendChild(lic_svr_cer);
			  
			  // set attribute to name element 
			 Element lic_svr_pfx =
			  doc.createElement("lic_svr_pfx"); lic_svr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_license_server.pfx"));
			  config.appendChild(lic_svr_pfx);
			  
			  // set attribute to name element 
			  Element lic_svr_pfx_pwd =
			  doc.createElement("lic_svr_pfx_pwd");
			  lic_svr_pfx_pwd.appendChild(doc.createTextNode("MZTPp2eoptw="));
			  config.appendChild(lic_svr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element pkgr_pfx =
			  doc.createElement("pkgr_pfx"); pkgr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_production_packager.pfx"));
			  config.appendChild(pkgr_pfx);
			  
			  // set attribute to name element 
			  Element pkgr_pfx_pwd =
			  doc.createElement("pkgr_pfx_pwd");
			  pkgr_pfx_pwd.appendChild(doc.createTextNode("MZTPp2eoptw="));
			  config.appendChild(pkgr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element transport_cer =
			  doc.createElement("transport_cer"); transport_cer
			  .appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_production_transport.der"));
			  config.appendChild(transport_cer);
			  
			  // set attribute to name element 
			  Element common_key_file =
			  doc.createElement("common_key_file");
			  common_key_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/common-key.bin"));
			  config.appendChild(common_key_file);
			  
			  // set attribute to name element 
			  Element policy_file =
			  doc.createElement("policy_file"); policy_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_policy.pol"));
			  config.appendChild(policy_file);
			  
			  // set attribute to name element 
			  Element rec_cer =
			  doc.createElement("rec_cer");
			  rec_cer.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/creds/sd"));
			  config.appendChild(rec_cer);
			  
			  // set attribute to name element 
			  Element content_id =
			  doc.createElement("content_id");
			  content_id.appendChild(doc.createTextNode("_default_"));
			  config.appendChild(content_id);
			  
			 /* // set attribute to name element 
			  Element key_rot =
			  doc.createElement("key_rot");
			  key_rot.appendChild(doc.createTextNode(""));
			  config.appendChild(key_rot);
			  
			  // set attribute to name element 
			  Element key_rot_dur =
			  doc.createElement("key_rot_dur");
			  key_rot_dur.appendChild(doc.createTextNode("8"));
			  config.appendChild(key_rot_dur);*/
			 

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			file = new File("/ivid/encodingService/offlinepkgr/" + vname + "_protected.xml");// Change the file name
																	
			StreamResult result = new StreamResult(file);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);


		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return null;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return null;
		}

		return file.getName();
	}
	
	/** 
     * aesXmlCreator method
     *
     * @param 			String vname
     * @return          String 
     * @see              
     * @version         1.0
     */	
	public String aesXmlCreator(String vname) {


		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element config = doc.createElement("config");
			doc.appendChild(config);
			
			// set Element attribute to in_path element
			Element in_path = doc.createElement("in_path");
			in_path.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/INPUT/" + vname + ".mp4"));
			config.appendChild(in_path);

			// set attribute to out_path element
			Element out_path = doc.createElement("out_path");
			out_path.appendChild(doc
					.createTextNode("/var/lib/tomcat7/webapps/Videos/Aes"));																					
			config.appendChild(out_path);

			// set attribute to out_type element
			Element out_type = doc.createElement("out_type");
			out_type.appendChild(doc.createTextNode("hls"));
			config.appendChild(out_type);
			
			// set attribute to name element
			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(vname));
			config.appendChild(name);

			// set attribute to drm element
			Element drm = doc.createElement("drm");
			config.appendChild(drm);

			// set attribute to name element
			Element drm_sys = doc.createElement("drm_sys");
			drm_sys.appendChild(doc.createTextNode("none"));
			config.appendChild(drm_sys);
						
			// set attribute to name element
			Element frag_dur = doc.createElement("frag_dur");
			frag_dur.appendChild(doc.createTextNode("4"));
			config.appendChild(frag_dur);

			// set attribute to name element
			Element target_dur = doc.createElement("target_dur");
			target_dur.appendChild(doc.createTextNode("6"));
			config.appendChild(target_dur);
			

			// set attribute to name element 
			Element lic_svr_url = doc.createElement("lic_svr_url");
			lic_svr_url.appendChild(doc
					.createTextNode("http://primetime.aaxs.adobe.com"));
			config.appendChild(lic_svr_url);
			  
			// set attribute to name element 
			Element lic_svr_cer = doc.createElement("lic_svr_cer");
			lic_svr_cer.appendChild(doc
					.createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_license_server.der"));
			config.appendChild(lic_svr_cer);
			  
			 // set attribute to name element 
			 Element lic_svr_pfx =
			  doc.createElement("lic_svr_pfx"); lic_svr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_license_server.pfx"));
			  config.appendChild(lic_svr_pfx);
			  
			  // set attribute to name element 
			  Element lic_svr_pfx_pwd =
			  doc.createElement("lic_svr_pfx_pwd");
			  lic_svr_pfx_pwd.appendChild(doc.createTextNode("MZTPp2eoptw="));
			  config.appendChild(lic_svr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element pkgr_pfx =
			  doc.createElement("pkgr_pfx"); pkgr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_production_packager.pfx"));
			  config.appendChild(pkgr_pfx);
			  
			  // set attribute to name element 
			  Element pkgr_pfx_pwd =
			  doc.createElement("pkgr_pfx_pwd");
			  pkgr_pfx_pwd.appendChild(doc.createTextNode("MZTPp2eoptw="));
			  config.appendChild(pkgr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element transport_cer =
			  doc.createElement("transport_cer"); 
			  transport_cer.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_production_transport.der"));
			  config.appendChild(transport_cer);
			  
			  // set attribute to name element 
			  Element common_key_file =
			  doc.createElement("common_key_file");
			  common_key_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/common-key.bin"));
			  config.appendChild(common_key_file);
			  
			  // set attribute to name element 
			  Element policy_file =
			  doc.createElement("policy_file"); policy_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/phds_policy.pol"));
			  config.appendChild(policy_file);
			  
			  // set attribute to name element 
			  Element rec_cer =
			  doc.createElement("rec_cer");
			  rec_cer.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/creds/sd"));
			  config.appendChild(rec_cer);
			  
			  // set attribute to name element 
			  Element content_id =
			  doc.createElement("content_id");
			  content_id.appendChild(doc.createTextNode("_default_"));
			  config.appendChild(content_id);
			  
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			file = new File("/ivid/encodingService/offlinepkgr/" + vname + "_aes.xml");// Change the file name dynamically...
			StreamResult result = new StreamResult(file);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return null;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return null;
		}
		return file.getName();
	}
	
	/** 
     * drmplainXmlCreator method
     *
     * @param 			String vname
     * @return          String 
     * @see              
     * @version         1.0
     */	
	public String drmplainXmlCreator(String vname) {


		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element config = doc.createElement("config");
			doc.appendChild(config);

			// set Element attribute to content_id element
			Element content_id = doc.createElement("content_id");
			content_id.appendChild(doc.createTextNode(vname));
			config.appendChild(content_id);
			
			// set Element attribute to in_path element
			Element in_path = doc.createElement("in_path");
			in_path.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/INPUT/" + vname + ".mp4"));
			config.appendChild(in_path);

			// set attribute to out_path element
			Element out_path = doc.createElement("out_path");
			out_path.appendChild(doc
					.createTextNode("/var/lib/tomcat7/webapps/Videos/Drmplain"));																					
			config.appendChild(out_path);

			// set attribute to out_type element
			Element out_type = doc.createElement("out_type");
			out_type.appendChild(doc.createTextNode("hls"));
			config.appendChild(out_type);

			// set attribute to name element
			Element frag_dur = doc.createElement("frag_dur");
			frag_dur.appendChild(doc.createTextNode("4"));
			config.appendChild(frag_dur);

			// set attribute to name element
			Element target_dur = doc.createElement("target_dur");
			target_dur.appendChild(doc.createTextNode("6"));
			config.appendChild(target_dur);
			
			// set attribute to name element
			Element drm = doc.createElement("drm");
			drm.appendChild(doc.createTextNode("true"));
			config.appendChild(drm);

			// set attribute to name element
			Element drm_sys = doc.createElement("drm_sys");
			drm_sys.appendChild(doc.createTextNode("AAXS"));
			config.appendChild(drm_sys);

			// set attribute to name element 
			Element lic_svr_url = doc.createElement("lic_svr_url");
			lic_svr_url.appendChild(doc
					.createTextNode("http://access.adobeprimetime.com/flashaccessserver/axs_prod"));
			config.appendChild(lic_svr_url);
			  
			// set attribute to name element 
			Element lic_svr_cer = doc.createElement("lic_svr_cer");
			lic_svr_cer.appendChild(doc
					.createTextNode("/ivid/encodingService/offlinepkgr/creds/static/clouddrm-license.cer"));
			config.appendChild(lic_svr_cer);
			  
			  // set attribute to name element 
			  Element pkgr_pfx =
			  doc.createElement("pkgr_pfx"); pkgr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/WT-license.pfx"));
			  config.appendChild(pkgr_pfx);
			  
			  // set attribute to name element 
			  Element pkgr_pfx_pwd =
			  doc.createElement("pkgr_pfx_pwd");
			  pkgr_pfx_pwd.appendChild(doc.createTextNode("wipro123"));
			  config.appendChild(pkgr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element transport_cer =
			  doc.createElement("transport_cer"); transport_cer
			  .appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/clouddrm-transport.cer"));
			  config.appendChild(transport_cer);
			  
			  // set attribute to name element 
			  Element common_key_file =
			  doc.createElement("common_key_file");
			  common_key_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/common-key.bin"));
			  config.appendChild(common_key_file);
			  
			  // set attribute to name element 
			  Element policy_file =
			  doc.createElement("policy_file"); policy_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/anonymous.pol"));
			  config.appendChild(policy_file);
			  
			  // set attribute to name element 
			  Element rec_cer =
			  doc.createElement("rec_cer");
			  rec_cer.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/creds/sd"));
			  config.appendChild(rec_cer);
			  
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			file = new File("/ivid/encodingService/offlinepkgr//" + vname + "_drmplain.xml");// Change the file name dynamically...
			StreamResult result = new StreamResult(file);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return null;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return null;
		}
		return file.getName();
	}
	
	/** 
     * drmNoWindowsXmlCreator method
     *
     * @param 			String vname
     * @return          String 
     * @see              
     * @version         1.0
     */	
	public String drmNoWindowsXmlCreator(String vname) {


		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element config = doc.createElement("config");
			doc.appendChild(config);

			// set Element attribute to content_id element
			Element content_id = doc.createElement("content_id");
			content_id.appendChild(doc.createTextNode(vname));
			config.appendChild(content_id);
			
			// set Element attribute to in_path element
			Element in_path = doc.createElement("in_path");
			in_path.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/INPUT/" + vname + ".mp4"));
			config.appendChild(in_path);

			// set attribute to out_path element
			Element out_path = doc.createElement("out_path");
			out_path.appendChild(doc
					.createTextNode("/var/lib/tomcat7/webapps/Videos/Drmnowindows"));																					
			config.appendChild(out_path);

			// set attribute to out_type element
			Element out_type = doc.createElement("out_type");
			out_type.appendChild(doc.createTextNode("hls"));
			config.appendChild(out_type);

			// set attribute to name element
			Element frag_dur = doc.createElement("frag_dur");
			frag_dur.appendChild(doc.createTextNode("4"));
			config.appendChild(frag_dur);

			// set attribute to name element
			Element target_dur = doc.createElement("target_dur");
			target_dur.appendChild(doc.createTextNode("6"));
			config.appendChild(target_dur);
			
			// set attribute to name element
			Element drm = doc.createElement("drm");
			drm.appendChild(doc.createTextNode("true"));
			config.appendChild(drm);

			// set attribute to name element
			Element drm_sys = doc.createElement("drm_sys");
			drm_sys.appendChild(doc.createTextNode("AAXS"));
			config.appendChild(drm_sys);

			// set attribute to name element 
			Element lic_svr_url = doc.createElement("lic_svr_url");
			lic_svr_url.appendChild(doc
					.createTextNode("http://access.adobeprimetime.com/flashaccessserver/axs_prod"));
			config.appendChild(lic_svr_url);
			  
			// set attribute to name element 
			Element lic_svr_cer = doc.createElement("lic_svr_cer");
			lic_svr_cer.appendChild(doc
					.createTextNode("/ivid/encodingService/offlinepkgr/creds/static/clouddrm-license.cer"));
			config.appendChild(lic_svr_cer);
			  
			  // set attribute to name element 
			  Element pkgr_pfx =
			  doc.createElement("pkgr_pfx"); pkgr_pfx.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/WT-license.pfx"));
			  config.appendChild(pkgr_pfx);
			  
			  // set attribute to name element 
			  Element pkgr_pfx_pwd =
			  doc.createElement("pkgr_pfx_pwd");
			  pkgr_pfx_pwd.appendChild(doc.createTextNode("wipro123"));
			  config.appendChild(pkgr_pfx_pwd);
			  
			  // set attribute to name element 
			  Element transport_cer =
			  doc.createElement("transport_cer"); transport_cer
			  .appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/clouddrm-transport.cer"));
			  config.appendChild(transport_cer);
			  
			  // set attribute to name element 
			  Element common_key_file =
			  doc.createElement("common_key_file");
			  common_key_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/common-key.bin"));
			  config.appendChild(common_key_file);
			  
			  // set attribute to name element 
			  Element policy_file =
			  doc.createElement("policy_file"); policy_file.appendChild(doc
			  .createTextNode("/ivid/encodingService/offlinepkgr/creds/static/WT_nowindows.pol"));
			  config.appendChild(policy_file);
			  
			  // set attribute to name element 
			  Element rec_cer =
			  doc.createElement("rec_cer");
			  rec_cer.appendChild(doc.createTextNode("/ivid/encodingService/offlinepkgr/creds/sd"));
			  config.appendChild(rec_cer);
			  
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			file = new File("/ivid/encodingService/offlinepkgr//" + vname + "_drmnowindows.xml");// Change the file name dynamically...
			StreamResult result = new StreamResult(file);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			return null;
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
			return null;
		}
		return file.getName();
	}
	
	/** 
     * Delete method
     *
     * @param convert   the image to be drawn
     * @return          true if all conversion process completed successfully. 
     *               	false if any type of error or exception occurred during
     *               	the conversion process.
     * @see             XmlConfig, ExecuteCommand, DAOForConversion, Thumbnail
     * @version         1.0
     */	
	public void delete() {
		
		if (file.exists()) {
			file.delete();
		} 
	}

	/*// Get the configuration file name.....
	public String getConfigFileName() {

		String filename = file.getName();

		return filename;
	}*/

}