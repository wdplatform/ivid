package com.wipro.encrypt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ExecuteCommand {

	synchronized public boolean executeCommand(String filename) {

		// Execute cmd Command to fragment video file
		String command = "cd /"
				+ System.getenv("VIDEO_LOG")
				+ "/ && java -jar  /ivid/encodingService/offlinepkgr/OfflinePackager.jar -conf_path "
				+ "/ivid/encodingService/offlinepkgr/"+filename;
		
		
		try{
		executeLinuxCommand(command);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private String executeLinuxCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
                            new BufferedReader(new InputStreamReader(p.getInputStream()));

                        String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}

}
